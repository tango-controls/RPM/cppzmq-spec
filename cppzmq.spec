# Header-only library.
%global debug_package %{nil}

Name:           cppzmq
Version:        4.7.1
Release:        1%{?dist}.maxlab
Summary:        Header-only C++ binding for libzmq

License:        MIT
URL:            https://zeromq.org
Source0:        https://github.com/zeromq/%{name}/archive/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:  cmake3
BuildRequires:  gcc-c++
BuildRequires:  pkgconfig(libzmq)

%global _description \
cppzmq is a C++ binding for libzmq. \
\
cppzmq maps the libzmq C API to C++ concepts. In particular, it is type-safe, \
provides exception-based error handling, and provides RAII-style classes that \
automate resource management. cppzmq is a light-weight, header-only binding.

%description %{_description}


%package devel
Summary:        %{summary}
Provides:       %{name}-static = %{version}-%{release}

Requires:       pkgconfig(libzmq)

%description devel %{_description}


%prep
%setup -q -n %{name}-%{version}


%build
%cmake3 -DENABLE_DRAFTS=OFF -DCPPZMQ_BUILD_TESTS=OFF .
%make_build


%install
%make_install


%check
ctest3 -V


%files devel
%doc README.md
%license LICENSE
%{_includedir}/zmq*.hpp
%{_datadir}/cmake/%{name}


%changelog
* Wed Dec 16 2020 Benjamin Bertrand <benjamin.bertrand@maxiv.lu.se> 4.7.1-1
- Version 4.7.1

* Tue Dec 15 2020 Benjamin Bertrand <benjamin.bertrand@maxiv.lu.se> 4.4.1-1
- Build for EPEL 7
