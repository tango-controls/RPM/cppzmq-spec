[![Copr build status](https://copr.fedorainfracloud.org/coprs/g/tango-controls/tango/package/cppzmq/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/g/tango-controls/tango/package/cppzmq/)

# cppzmq

The cppzmq package

This is a forked from https://src.fedoraproject.org/rpms/cppzmq to build for EPEL 7

Package is available in `@tango-controls/tango` project in Copr: <https://copr.fedorainfracloud.org/coprs/g/tango-controls/tango/package/cppzmq/>
